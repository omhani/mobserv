package com.example.root.location_access;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements LocationListener {
    Button getLocationBtn;
    TextView locationText;

    LocationManager locationManager;
    Location location;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getLocationBtn = (Button) findViewById(R.id.getLocationBtn);
        locationText = (TextView) findViewById(R.id.locationText);
        getLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "yyes it works", Toast.LENGTH_SHORT).show();
                Log.d("hello", "it works");
                getLocation();

            }

        });
    }

        void getLocation (){
            try {
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                //update every 5 m,5 sec
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                Log.d("getlocation", "it works");
                locationText.setText("Current Location: " + location.getLatitude() + ", " + location.getLongitude());
                Log.i("Location in try",location.toString());

            }
            catch(SecurityException e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "problem", Toast.LENGTH_SHORT).show();

            }
        }


    @Override
    public void onLocationChanged(Location location1) {
        location = location1;

        Log.i("Location",location.toString());

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(MainActivity.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();

    }




}
