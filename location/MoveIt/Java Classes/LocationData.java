package eurecom.fr.android.test1000;

/**
 * Created by OMHANI on 12/19/2017.
 */


public class LocationData {

    double latitude;
    double longitude;

    public LocationData(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
