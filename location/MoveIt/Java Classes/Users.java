package eurecom.fr.android.test1000;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Users extends AppCompatActivity {
    Button btnsender ,btntransporter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        init();

        btnsender.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent sendi=new Intent(Users.this, Sender.class) ;

                startActivity(sendi);

            }
        });
        btntransporter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i("working ", "Button Transporter ***************--------------");

                Intent transi=new Intent(Users.this, Transporter.class) ;

                startActivity(transi);

            }
        });
    }
    public void init () {
        btnsender = (Button) findViewById(R.id.btnsender);
        btntransporter = (Button) findViewById(R.id.btntransporter);
    }

}
